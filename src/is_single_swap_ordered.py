def is_single_swap_ordered(list_to_check: list) -> bool:
    """
    A function that when provided a list will return a boolean for if the list can be sorted with a single swap of
    elements.

    :param list_to_check: (List) - The list to check for sorting with a single swap.
    :return: (Bool) - A True or False indicator for if the list can be sorted with a single swap.
    """
    sorted_list = sorted(list_to_check)
    mismatches = filter(lambda x: x[0] != x[1], zip(list_to_check, sorted_list))
    if len(list(mismatches)) < 3:
        return True
    return False
