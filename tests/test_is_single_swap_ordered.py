from unittest import TestCase

from src.is_single_swap_ordered import is_single_swap_ordered


class TestIsSingleSwapOrdered(TestCase):

    def test_happy_path(self):
        self.assertTrue(is_single_swap_ordered([1, 5, 100, 99]))

    def test_duplicate_numbers(self):
        self.assertTrue(is_single_swap_ordered([1, 5, 3, 3, 7]))

    def test_ordered_list(self):
        self.assertTrue(is_single_swap_ordered([1, 3, 5]))

    def test_two_item_list_that_is_sorted_by_swapping_items(self):
        self.assertTrue(is_single_swap_ordered([5, 1]))

    def test_single_item_list(self):
        self.assertTrue(is_single_swap_ordered([5]))

    def test_list_that_where_swap_creates_second_unordered_list(self):
        self.assertFalse(is_single_swap_ordered([1, 3, 5, 3, 4]))

    def test_list_that_can_be_sorted_with_deque_but_not_swap(self):
        self.assertFalse(is_single_swap_ordered([5, 1, 2, 3]))

    def test_list_with_multiple_individual_swaps(self):
        self.assertFalse(is_single_swap_ordered([1, 5, 3, 3, 7, 6, 8, 9]))
